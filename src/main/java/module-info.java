/**
 * Added the "open" keywoard tot he module. This way, we do not need
 * to add specific "opens"-sections for each of the packages in the project.
 */
open module JPA.Contacts {
  requires javafx.controls;
  requires javafx.fxml;
  requires java.logging;
  requires jakarta.persistence;
  //opens no.ntnu.idata2001.contacts.views to javafx.graphics;
  //opens no.ntnu.idata2001.contacts.model to java.persistence;
  exports no.ntnu.idata2001.contacts;
}