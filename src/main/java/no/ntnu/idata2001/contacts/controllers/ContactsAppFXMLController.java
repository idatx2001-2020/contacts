package no.ntnu.idata2001.contacts.controllers;

import java.io.File;
import java.io.IOException;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.FileChooser;
import javafx.util.Pair;
import no.ntnu.idata2001.contacts.AppVersion;
import no.ntnu.idata2001.contacts.model.AddressBook;
import no.ntnu.idata2001.contacts.model.AddressBookDBHandler;
import no.ntnu.idata2001.contacts.model.AddressBookFileHandler;
import no.ntnu.idata2001.contacts.model.AddressBookPlain;
import no.ntnu.idata2001.contacts.model.ContactDetails;
import no.ntnu.idata2001.contacts.views.ContactDetailsDialog;

/**
 * The controller class mapped to the FXML main class.
 *
 * <p>A few words on the @FXML-annotation:
 * The @FXML annotation is used to enable initialization of fields performed
 * by the FXML-loader. For example, in the code below, there is a field
 * for the TableView. The instance to be created, will be created by the FXML loader,
 * hence you should NEVER create this instance here in the controller.
 *
 * <p>Now, if the field (like contactDetailsTableView) is public, you do not need to
 * use the annotation @FXML on the field for it to be initialized. BUT if you set it
 * to private (which you should, according to good practice), you MUST add the @FXML
 * annotation for the field to be initialized by the FXML loader.
 * See: https://stackoverflow.com/questions/30210170/is-fxml-needed-for-every-declaration
 */
public class ContactsAppFXMLController {
  private final Logger logger = Logger.getLogger(getClass().toString());

  @FXML
  private TableView<ContactDetails> contactDetailsTableView;

  // The observable list to be used with the TableView
  ObservableList<ContactDetails> observableContactsList;
  // The address book to be used to store the contact details in.
  private AddressBook addressBook;

  /**
   * This method will be called by the FXML-loader after having loaded
   * all the GUI-components and initialized them.
   */
  @FXML
  private void initialize() {
    this.observableContactsList = FXCollections.observableArrayList();
    this.addressBook = new AddressBookDBHandler();
    //this.addressBook = new AddressBookPlain();
    this.observableContactsList.setAll(this.addressBook.getAllContacts());
    this.contactDetailsTableView.setItems(observableContactsList);
  }

  /**
   * Updates the ObservableArray wrapper with the current content in the
   * Literature register. Call this method whenever changes are made to the
   * underlying LiteratureRegister.
   *
   * @param addressBook the address book to use for updating the observable list
   */
  public void updateObservableList(AddressBook addressBook) {
    this.observableContactsList.setAll(addressBook.getAllContacts());
  }

  /**
   * Display the input dialog to get input to create a new Contact.
   * If the user confirms creating a new contact, a new instance
   * of ContactDetails is created and added to the AddressBook provided.
   *
   * @param actionEvent the actionevent triggering this call
   */
  public void addContact(ActionEvent actionEvent) {
    ContactDetailsDialog contactsDialog = new ContactDetailsDialog();
    Optional<ContactDetails> result = contactsDialog.showAndWait();

    if (result.isPresent()) {
      ContactDetails newContactDetails = result.get();
      this.addressBook.addContact(newContactDetails);
      this.updateObservableList(this.addressBook);
    }
  }

  /**
   * Import contacts from a .CSV-file chosen by the user.
   *
   * @param actionEvent the actionevent triggering this call
   */
  public void importFromCSV(ActionEvent actionEvent) {
    FileChooser fileChooser = new FileChooser();

    // Set extension filter for .csv-file
    FileChooser.ExtensionFilter extFilter =
        new FileChooser.ExtensionFilter("CSV files (*.csv)", "*.csv");
    fileChooser.getExtensionFilters().add(extFilter);

    // Show save open dialog
    File file = fileChooser.showOpenDialog(null);
    if (file != null) {
      try {
        AddressBookFileHandler.importFromCsv(this.addressBook, file);
        this.updateObservableList(this.addressBook);
      } catch (IOException ioe) {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("File Import Error");
        alert.setHeaderText("Error during CSV-import.");
        alert.setContentText("Details: " + ioe.getMessage());
        alert.showAndWait();
      }
    }
  }

  /**
   * Export all contacts in the address book to a CSV-file specified by the user.
   *
   * @param actionEvent the actionevent triggering this call
   */
  public void exportToCSV(ActionEvent actionEvent) {
    FileChooser fileChooser = new FileChooser();

    // Set extension filter for .csv-file
    FileChooser.ExtensionFilter extFilter =
        new FileChooser.ExtensionFilter("CSV files (*.csv)", "*.csv");
    fileChooser.getExtensionFilters().add(extFilter);

    // Show save file dialog
    File file = fileChooser.showSaveDialog(null);
    if (file != null) {
      try {
        AddressBookFileHandler.exportToCsv(this.addressBook, file);
      } catch (IOException ioe) {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("File Export Error");
        alert.setHeaderText("Error during CSV-export.");
        alert.setContentText("Details: " + ioe.getMessage());
        alert.showAndWait();
      }
    }
  }

  /**
   * Exit the application. Displays a confirmation dialog.
   *
   * @param actionEvent the actionevent triggering this call
   */
  public void exitApplication(ActionEvent actionEvent) {
    Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
    alert.setTitle("Confirmation Dialog");
    alert.setHeaderText("Exit Application ?");
    alert.setContentText("Are you sure you want to exit this application?");

    Optional<ButtonType> result = alert.showAndWait();

    if (result.isPresent() && (result.get() == ButtonType.OK)) {
      // ... user choose OK
      Platform.exit();
    }
  }

  /**
   * Removes the Contact selected in the table. If no Contact is
   * selected, nothing is deleted, and the user is informed that he/she must
   * select which Contact to delete.
   *
   * @param actionEvent the actionevent triggering this call
   */
  public void removeContact(ActionEvent actionEvent) {
    ContactDetails selectedContact = this.contactDetailsTableView
        .getSelectionModel()
        .getSelectedItem();
    if (selectedContact == null) {
      showPleaseSelectItemDialog();
    } else {
      if (showDeleteConfirmationDialog()) {
        this.addressBook.removeContact(selectedContact.getPhone());
        this.updateObservableList(this.addressBook);
      }
    }
  }

  /**
   * Edit the selected item.
   *
   * @param actionEvent the actionevent triggering this call
   */
  public void editContact(ActionEvent actionEvent) {
    ContactDetails selectedContact = this.contactDetailsTableView
        .getSelectionModel()
        .getSelectedItem();
    if (selectedContact == null) {
      showPleaseSelectItemDialog();
    } else {
      ContactDetailsDialog contactDialog = new ContactDetailsDialog(selectedContact, true);
      contactDialog.showAndWait();

      // The selectedContact now contains the updates. If the address book is
      // stored in memory (implemented using one of the collection classes in the JDK
      // or similar), we do not need to tell the address book that this instance
      // has been changed. But if a DB is being used, the updated details
      // must be stored/updeted in the DB too. Hence lets treat this change
      // independent upon the address book being "in memory" or in a DB by
      // calling the update()-method of the address book
      this.addressBook.updateContact(selectedContact);

      this.updateObservableList(this.addressBook);
    }
  }


  /**
   * Displays an example of an alert (info) dialog. In this case an "about"
   * type of dialog.
   *
   * @param actionEvent the actionevent triggering this call
   */
  public void showAboutDialog(ActionEvent actionEvent) {
    Alert alert = new Alert(Alert.AlertType.INFORMATION);
    alert.setTitle("Information Dialog - About");
    alert.setHeaderText("Contacts Register\nv" + AppVersion.VERSION);
    alert.setContentText("A brilliant application created by\n"
        + "(C)Arne Styve\n"
        + "2020-03-16");

    alert.showAndWait();
  }

  /**
   * Displays a login dialog using a custom dialog.
   * Just to demonstrate the {@link javafx.scene.control.PasswordField}-control.
   *
   * @param actionEvent the actionevent triggering this call
   */
  public void showLoginDialog(ActionEvent actionEvent) {
    // Create the custom dialog.
    Dialog<Pair<String, String>> dialog = new Dialog<>();
    dialog.setTitle("Login Dialog");
    dialog.setHeaderText("Look, a Custom Login Dialog");

    // Set the button types.
    ButtonType loginButtonType = new ButtonType("Login", ButtonBar.ButtonData.OK_DONE);
    dialog.getDialogPane().getButtonTypes().addAll(loginButtonType, ButtonType.CANCEL);

    // Create the username and password labels and fields.
    GridPane grid = new GridPane();
    grid.setHgap(10);
    grid.setVgap(10);
    grid.setPadding(new Insets(20, 150, 10, 10));

    TextField username = new TextField();
    username.setPromptText("Username");

    PasswordField password = new PasswordField();
    password.setPromptText("Password");

    grid.add(new Label("Username:"), 0, 0);
    grid.add(username, 1, 0);
    grid.add(new Label("Password:"), 0, 1);
    grid.add(password, 1, 1);

    // Enable/Disable login button depending on whether a username was entered.
    Node loginButton = dialog.getDialogPane().lookupButton(loginButtonType);
    loginButton.setDisable(true);

    // Do some validation .
    username.textProperty().addListener((observable, oldValue, newValue) ->
        loginButton.setDisable(newValue.trim().isEmpty()));

    dialog.getDialogPane().setContent(grid);

    // Request focus on the username field by default.
    Platform.runLater(username::requestFocus);

    // Convert the result to a username-password-pair when the login button is clicked.
    dialog.setResultConverter(
        dialogButton -> {
          if (dialogButton == loginButtonType) {
            return new Pair<>(username.getText(), password.getText());
          }
          return null;
        });

    Optional<Pair<String, String>> result = dialog.showAndWait();

    result.ifPresent(
        usernamePassword -> logger.log(Level.INFO, () -> "Username=" + usernamePassword.getKey()
            + ", Password=" + usernamePassword.getValue()));
  }

  /**
   * Displays a warning informing the user that an item must be selected from
   * the table.
   */
  public void showPleaseSelectItemDialog() {
    Alert alert = new Alert(Alert.AlertType.WARNING);
    alert.setTitle("Information");
    alert.setHeaderText("No items selected");
    alert.setContentText("No item is selected from the table.\n"
        + "Please select an item from the table.");

    alert.showAndWait();
  }

  /**
   * Displays a delete confirmation dialog. If the user confirms the delete,
   * <code>true</code> is returned.
   *
   * @return <code>true</code> if the user confirms the delete
   */
  public boolean showDeleteConfirmationDialog() {
    boolean deleteConfirmed = false;

    Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
    alert.setTitle("Delete confirmation");
    alert.setHeaderText("Delete confirmation");
    alert.setContentText("Are you sure you want to delete this item?");

    Optional<ButtonType> result = alert.showAndWait();

    if (result.isPresent()) {
      deleteConfirmed = (result.get() == ButtonType.OK);
    }
    return deleteConfirmed;
  }
}
