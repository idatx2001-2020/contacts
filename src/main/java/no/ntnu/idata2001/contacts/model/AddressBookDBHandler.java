package no.ntnu.idata2001.contacts.model;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.Persistence;
import jakarta.persistence.Query;


/**
 * <p>An addressbook holding contacts, stored in a relational database, implemented using
 * the JPA implementation EclipseLink.
 * Using the JPA standard for Object Relational Mapping (ORM) removes database specifics
 * from the Java code, and hides the underlying RDB solution, making it easy to change between
 * different suppliers of Relational Database management Systems (RDBMS), and between locally
 * embedded database and server based database systems.
 * JPA makes use of <b>EntityManager</b> (EMF) and <b>EntityManagerFactory</b> (EM) classes for
 * communication with the underlying RDBMS. Creating an EntityManagerFactory instance is costly:</p>
 *
 * <p>(<a href="https://docs.jboss.org/hibernate/entitymanager/3.5/reference/en/html_single/#d0e980">)
 * "An entity manager factory is typically create at application initialization time and closed at
 * * application end. It's creation is an expensive process."</a></p>
 * <p>
 * Hence the EMF is implemented as a field in this class.
 * </p>
 * <p><b>NOTE:</b> While the EMF is <b>thread safe</b>, the EM is <b>not thread safe</b>.</p>
 */
public class AddressBookDBHandler implements AddressBook {

  // Note the 'transient'-keyword. This is to indicate that this field
  // should never be serialized. Since the AddressBook-interface
  // implements Serializable (used for object serialisation), this class
  // too is tagged to be serializable. But the field in this class is used to
  // communicate with the database, and should hence not be serialized.
  private final transient EntityManagerFactory efact;

  // Create a logger to be used within this class
  private final transient Logger logger = Logger.getLogger(this.getClass().getName());

  /**
   * Creates an instance of the AddressBookDBHandler.
   */
  public AddressBookDBHandler() {
    this.efact = Persistence.createEntityManagerFactory("contacts-pu");
    //    this.efact = Persistence.createEntityManagerFactory("contacts-mysql-pu");
    //    this.efact = Persistence.createEntityManagerFactory("contacts-localdb-pu");

  }

  @Override
  public void addContact(ContactDetails contact) {
    EntityManager eman = this.efact.createEntityManager();
    eman.getTransaction().begin();
    eman.persist(contact);
    eman.getTransaction().commit();
    eman.close();

    this.logger.log(Level.INFO, () ->
        "A contact was added to the DB. Name = "
            + contact.getName() + " Phone = "
            + contact.getPhone());
  }

  @Override
  public void removeContact(String phoneNumber) {
    EntityManager eman = this.efact.createEntityManager();

    eman.getTransaction().begin();

    String sql = "DELETE FROM ContactDetails c WHERE c.phone LIKE :contactPhone";

    Query query = eman.createQuery(sql).setParameter("contactPhone", phoneNumber);
    int n = query.executeUpdate();

    eman.getTransaction().commit();

    // A quick note on this way of logging a message where we need to use
    // string concatenation: String concatenation is "expensive". Hence we should
    // avoid having to build the string if it is not going to be used, i.e. if the
    // logging level is set to lower than INFO, this log-message will never be logged
    // even though the string concatenation always will been performed.
    // One solution, is to use lambda like shown below. This way we ensure that the lambda
    // expression will not be executed unless the logging level is set to INFO or higher.
    // Another alternative is to use a formatter.
    this.logger.log(Level.INFO, () -> "Removed contact with phone = "
        + phoneNumber + ". EM returned " + n + " object deleted.");

    eman.close();

  }

  @Override
  public void updateContact(ContactDetails contactDetails) {
    EntityManager eman = this.efact.createEntityManager();
    eman.getTransaction().begin();

    // First find the contact details matching the unique id of the
    // given contact (cannot use the phone number, since that
    // might have changed....)
    ContactDetails contactToUpdate = eman.find(ContactDetails.class, contactDetails.getId());

    if (contactToUpdate != null) {
      // Use the updateFrom() method of the ContactDetails-class to
      // update the entity in the DB with the data from the parameter contactDetails
      contactToUpdate.updateFrom(contactDetails);
      eman.getTransaction().commit();
    }
    eman.close();
  }

  @Override
  public Collection<ContactDetails> getAllContacts() {
    EntityManager eman = this.efact.createEntityManager();

    String sql = "SELECT c FROM ContactDetails c";

    Query query = eman.createQuery(sql);

    List<ContactDetails> contactsList = query.getResultList();

    eman.close();

    this.logger.log(Level.INFO, () ->
        "Read all contacts from the DB, a total of " + contactsList.size());
    return contactsList;
  }

  @Override
  public void close() {
    this.efact.close();
  }

  @Override
  public Iterator<ContactDetails> iterator() {
    return getAllContacts().iterator();
  }
}
