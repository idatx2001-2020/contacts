package no.ntnu.idata2001.contacts.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.TreeMap;

/**
 * Represents an Address book containing contacts with contact details.
 * Based on the example in the book "Objects first with Java" by David J. Barnes
 * and Michael K&ouml;lling.
 *
 * <p>Each contact is stored in an ArrayList. We could have used
 * a HasMap or a TreeMap using the phone number as the key, but since the phone
 * number might change for a given contact, it is not advisable to use the
 * phone number as a key, and hence no need to use a Map to store the contact details.
 *
 * @author David J. Barnes and Michael K&ouml;lling and Arne Styve
 * @version 2020.03.16
 */
public class AddressBookPlain implements AddressBook {
  // Storage for an arbitrary number of details.
  // We have chosen to use TreeMap instead of HashMap in this example, the
  // main difference being that a TreeMap is sorted. That is, the keys are sorted,
  // so when retrieving an Iterator from a TreeMap-collection, the iterator will
  // iterate in a sorted manner, which wil not be the case for a HashMap.
  // TreeMap is a bit less efficient than a HashMap in terms of searching, du to the
  // sorted order. For more details on the difference:
  // https://javatutorial.net/difference-between-hashmap-and-treemap-in-java
  private final ArrayList<ContactDetails> book;

  /**
   * Creates an instance of the AddressBook, initialising the instance.
   */
  public AddressBookPlain() {
    this.book = new ArrayList<>();
  }

  /**
   * Add a new contact to the address book.
   *
   * @param contact The contact to be added.
   */
  public void addContact(ContactDetails contact) {
    if (contact != null) {
      book.add(contact);
    }
  }

  /**
   * Remove the contact with the given phone number from the address book.
   * The phone number should be one that is currently in use.
   *
   * @param phoneNumber The phone number to the contact to remove
   */
  public void removeContact(String phoneNumber) {
    // First find the contact details with the given phone number
    ContactDetails contactDetails = findContactByPhone(phoneNumber);
    if (contactDetails != null) {
      this.book.remove(contactDetails);
    }
  }

  /**
   * Finds the contact details matching the given phone number.
   * If no match is found, <code>null</code> is returned.
   * @param phoneNumber the phone number to search for
   * @return the contact details matching the phone number provided.
   *         If no match, <code>null</code> is returned.
   */
  private ContactDetails findContactByPhone(String phoneNumber) {
    ContactDetails foundContact = null;
    for (ContactDetails contactDetails : this.book) {
      if (contactDetails.getPhone().equals(phoneNumber)) {
        foundContact = contactDetails;
      }
    }
    return foundContact;
  }

  @Override
  public void updateContact(ContactDetails contactDetails) {
    // There is really no need to do anything with regards
    // to the ArrayList storing the contact details, as
    // long as the contactDetaisl-object provided in the
    // parameter is still within the ArrayList.
    // But to be sure, we will verify that the object IS in the
    // ArrayList, and if not, we will add it, just in case.

    if (!this.book.contains(contactDetails)) {
      this.book.add(contactDetails);
    }
  }

  /**
   * Returns all the contacts as a collection.
   *
   * @return all the contacts as a collection.
   */
  public Collection<ContactDetails> getAllContacts() {
    return this.book;
  }

  @Override
  public void close() {
    // Left empty intentionally. Nothing to close in this class.
  }

  @Override
  public Iterator<ContactDetails> iterator() {
    return this.book.iterator();
  }
}
