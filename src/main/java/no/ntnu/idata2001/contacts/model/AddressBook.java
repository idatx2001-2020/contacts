package no.ntnu.idata2001.contacts.model;

import java.io.Serializable;
import java.util.Collection;
import java.util.Iterator;

/**
 * Defines the interface to an address book. Provides methods for adding contacts,
 * deleting contacts and iterating over contacts.
 */
public interface AddressBook extends Serializable, Iterable<ContactDetails> {

  /**
   * Add a new contact to the address book.
   *
   * @param contact The contact to be added.
   */
  void addContact(ContactDetails contact);

  /**
   * Remove the contact with the given phone number from the address book.
   * The phone number should be one that is currently in use.
   *
   * @param phoneNumber The phone number to the contact to remove
   */
  void removeContact(String phoneNumber);

  /**
   * Update the contact info for the given contact.
   *
   * @param contactDetails the contact details to update.
   */
  void updateContact(ContactDetails contactDetails);

  /**
   * Returns all the contacts as a collection.
   *
   * @return all the contacts as a collection.
   */
  Collection<ContactDetails> getAllContacts();

  /**
   * Close the data source.
   */
  void close();

  @Override
  Iterator<ContactDetails> iterator();
}
