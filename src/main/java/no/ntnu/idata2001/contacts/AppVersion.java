package no.ntnu.idata2001.contacts;

/**
 * A simple class to hold the version of the application.
 *
 * <p>Using the "final" keyword on a class prevents the class to
 * be subclassed (inherited from), which makes sense in this case.
 * The field is also being set to final to indicate that there are to be
 * no changes to the field (i.e. a constant). Setting the field to
 * static creates a "class field", i.e. a field that exsists without having to
 * create an instance of the class.
 * To access the VERSION-constant, use:
 * <code>AppVersion.VERSION</code>
 */
public final class AppVersion {
  public static final String VERSION = "0.7";
}
