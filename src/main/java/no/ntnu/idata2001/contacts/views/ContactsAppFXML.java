package no.ntnu.idata2001.contacts.views;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class ContactsAppFXML extends Application {

  /**
   * The main entry point for the application, launching the
   * JavaFX app.
   * @param args command line arguments
   */
  public static void main(String[] args) {
    launch(args);
  }

  @Override
  public void start(Stage primaryStage) throws Exception {
    FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("ContactsMain.fxml"));

    Parent root = fxmlLoader.load();

    primaryStage.setTitle("Hello World");
    Scene scene = new Scene(root, 300, 275);
    scene.getStylesheets().add(ContactsAppFXML.class
        .getResource("ContactsAppFXML.css")
        .toExternalForm());
    primaryStage.setScene(scene);
    primaryStage.show();
  }
}
