# Contacts - an address book project

## Introduction
A JavaFX-project of a typical address book application. Used to demonstrate JavaFX, GUI design, use of the 
TableView-component and the mapping of the TableView to the backend register. Also includes example of file handling.

The project was first developed for use in teaching in the course "IDATx2001 Programmering 2" at NTNU spring 2020. 
In 2021 the project was converted to Maven and upgraded to Java 16, including use of modules.

![Screen dump](Screendump.png)

## Release notes

**Version** | **Description**
--------|------------
v0.7 | Converted the project into a **Maven** project. Also updated from Java 9 to Java 16, and included modules.
v0.6 | Added an alternative JavaFX-GUI based on FXML to give an example of how the project could be solved using FXML and CSS. To run the FXML-version, run the class **ContactsAppFXML**. This class uses the **ContactsMain.fxml**-file to build the GUI, together with the **ContactsAppFXML.css**-file. The controller used with the FXML-version of the GUI, is the **ContactsAppFXMLController**. <br>NOTE: Using "FXML" as part of the class-names is not a recommended practice, and is only done so in this project to make it clear which of the classes/files are linked to the use of the JavaFx FXML-solution. <br>Also, the existing non FXML-controller class **MainController** has been refactored to take the responsibility for the AddressBook instance (previously managed by the view class **ContactsApp**. <br>And there are also some other minor changes/modifications.
v0.5.1 | Some minor adjustments: removed *synchronized* from the iterator()-method of ContactDetails (not needed). Updated the ContactDetails-constructor to make use of the set-methods of the class.
v0.5 | Added support for MySQL-DB at IDI (https://mysql-ait.stud.idi.ntnu.no/phpmyadmin/) through a separate *persistence unit (PU)* in the persistence.xml-file.   NOTE: You must set your own **username**, **password** and **database name**.  Also support has been added for a locally installed Apache Derby Server.
v0.4.3 | Added support for MySQL-DB at IDI (https://mysql-ait.stud.idi.ntnu.no/phpmyadmin/) through a separate *persistence unit (PU)* in the persistence.xml-file.   NOTE: You must set your own **username**, **password** and **database name**.  Also support has been added for a locally installed Apache Derby Server.
v0.4.1 | Renamed the class AddressBookDAO to AddressBookDBHandler, since DAO is a general term that also could be used for the AddressBookFileHandler. Also altered slightly the use of EntityManager.
v0.4 | Added Relational Database support, using the embedded Apache Derby server. For details of the changes made, se below.
v0.3 | Adds object serialization of the entire address book. 
v0.2 | Added import and export from/to CSV-file. A default CSV-file is provided (Addresses.csv)
v0.1 | First release with basic add, edit, delete functionality.

### Release v0.4
The following changes were made to enable support for a Relational Database server (using the **embedded** mode of
 the [Apache Derby RDBMS](https://db.apache.org/derby/), using the [EclipseLink](https://www.eclipse.org/eclipselink/)
  persistence provider (JPA):
* **Added libraries** - Added a new folder, called *"libs"* in the IntelliJ project. Copied the **eclipselink.jar** 
 and the **jakarta.persistence_2.2.3.jar** files to the *libs*-folder for the **EclipseLink**-JPA support. Added
  **derby.jar** to the *libs*-folder for the Apache Derby embedded support. 
* **persistence.xml** - Added this file (used by the JPA-framework EclipseLink) to a folder named "**META-INF**", which 
is the same folder that the **MANIFEST.MF** should be. 
* **ContactDetails**-class: Added ORM/JPA specific annotations, like *@Entity*, *@Id* etc. Also, it must provide a
 default constructor with no parameters.
* **AddressBook** - interface: Added an Interface named *AddressBook*, renamed the exsisting *AddressBook* to
  *AddressBookPlain*. (Used the IntelliJ *Refactoring*: Extract Interface..)
* Added a new class **AddressBookDAO** which implements the interface AddressBook. This is the **Data Access Object** 
(DAO) providing the interface to the database. The class maps the methods defined in the interface to database
 actions.
 
 

## Description of the project
The project is made to demonstrate a typical application with a graphical user interface (GUI) implemented in JavaFX.
It's a classic address book example, where you can create contacts to be added to the address book, edit existing contacts, and delete contacts.
The project does **not** make use of FXML and SceneBuilder, but builds the GUI from within the Java code.
 
 #### JavaFX concepts demonstrated in the project
 The project uses JDK 8 for simplicity, since JavaFX was bundled with the JDK up to and including JDK 8. After JDK 8
 , JavaFX (and other packages) were moved into *modules* making it a bit more complex to create JavaFX based
  applications.
  
  The following JavaFX concepts are demonstrated in this project:
  * General JavaFX structure: Stage, Scene, Scenegraph, Nodes etc.
  * Event handling, using Lambda
  * Menu bar, toolbar, status bar
  * Buttons with icons
  * MenuItems with icon and keyboard shortcuts.
  * TableView for displaying a table, linking the table view to the backend AddressBook without adding JavaFX
   specific types to neither the AddressBook-class not the ContactDetails-class
  * Model-View-Controller: A separate Controller-class has been created performing the typical controller role in a
   typical MVC setting. The MVC is reflected in the package names in the project. **NOTE:** we have used "model" as the
    name of the package containing those classes that makes up the model-type of functionality. In a "real life
    " industry project, you would never use a single package named model (or even "view" for that matter). The package
     names should reflect the *functionality* and *services* provided by the classes in the package, and not the
      architecture of the application. In this project, the packages *view, model* and *controllers* is used for
       pedagogical purposes.
  * File-handling: Import/Export to CSV text file, in addition to object serialization used to provide persistence
   between sessions. Whenever the application closes down, the address book is saved to the file "addresses.dat
   ". Upon startup, the same file is being read to re-create the content of the address book from last session.
   
 ## Quality control of code and style
 The project has used both [CheckStyle](https://checkstyle.sourceforge.io/) and the [SonarLint](https://www.sonarlint
 .org/) plugin to [IntelliJ](https://www.jetbrains.com/idea/) for code quality and coding style. For coding style
 , the checkstyle [checks from Google](https://checkstyle.sourceforge.io/google_style.html) have been used.
  